import { Controller, Post, Body, Get, Param, Patch, Delete } from '@nestjs/common';
import { ProductsService } from './products.service';
import { title } from 'process';

@Controller('products')
export class ProductsController {
    constructor(private productsService: ProductsService) {}

    @Post()
    addProduct(
        @Body('title') title: string,
        @Body('description') description: string,
        @Body('price') price: number,
    ): any {
        const generated = this.productsService.insertProduct(title, description, price);
        return {data: generated};
    }

    @Get()
    getProducts()  {
        return this.productsService.getProducts();
    }

    @Get(':id')
    getOneProduct(@Param('id') id: string) {
        return this.productsService.getSingleProduct(id);
    }

    @Patch(':id')
    updateProduct(
        @Param('id') id: string,
        @Body('title') title: string,
        @Body('description') description: string,
        @Body('price') price: number
    ) {
        return this.productsService.updateProduct(id, title, description, price);
    }

    @Delete(':id')
    deleteProduct(@Param('id') id: string) {
        this.productsService.deleteProduct(id);
        return {data: 'Deleted'}
    }

   
}
