import { Injectable, NotFoundException } from '@nestjs/common';
import { Product } from './product.model';

@Injectable()
export class ProductsService {
    private products: Product[] = [];

    insertProduct(title: string, description: string, price: number) {
        const newProduct = new Product(
            Math.round(Math.random() * 10000).toString(),
            title, 
            description,
            price
        );

        this.products.push(newProduct);
        return newProduct;
    }

    getProducts() {
        return [...this.products];
    }

    getSingleProduct(id: string) {
        return this.findProduct(id)[0];
    }

    updateProduct(id: string, title: string, description: string, price: number) {
        const [product, index] = this.findProduct(id);
        const updated = {...product, title, description, price};
        this.products[index] = updated;
        return updated;
    }

    deleteProduct(id: string) {
        //just get index
        const [_ , index] = this.findProduct(id);
        this.products.splice(index, 1);
        console.log(this.products.length, index);
    }

    findProduct(id: string): [Product, number]{
        const index = this.products.findIndex(product => product.id === id);
        const product = this.products[index];
        if (!product) {
            throw new NotFoundException('Could not found product');
        }
        return [product, index];
    }
}
